package net.qqxh.controller;

import net.qqxh.controller.common.ResponseJsonFactory;
import net.qqxh.persistent.JfUserSimple;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseController{
    @Autowired
    public ResponseJsonFactory responseJsonFactory;
    public JfUserSimple getLoginUser(){
        JfUserSimple JfUserSimple = (JfUserSimple) SecurityUtils.getSubject().getPrincipal();
        return JfUserSimple;
    }
}
