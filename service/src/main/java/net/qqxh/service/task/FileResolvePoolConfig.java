package net.qqxh.service.task;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

@EnableAsync
@Configuration
public class FileResolvePoolConfig {
    @Value("${jodconverter.portNumbers}")
    private  String  jodport;
    @Value("${jodconverter.maxTasksPerProcess}")
    private int maxTasksPerProcess;

    @Bean("fileResolveExecutor")
    public Executor taskExecutor() {
        /*最大线程数量不能大于。liboffice总处理能力*/
        int maxPoolSize=jodport.split(",").length;
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        /*基本线程数*/
        executor.setCorePoolSize(maxPoolSize);
        /*最大线程数，为6是经验值*/
        executor.setMaxPoolSize(maxPoolSize+3);
        /*队列最大长度*/
        executor.setQueueCapacity(200);
        /*空闲退出时间*/
        executor.setKeepAliveSeconds(60);
        executor.setThreadNamePrefix("fileResolveExecutor-");
        /*哪个线程调这个任务就在哪个线程里执行，主线程停止for循环，先来把这个任务做了，然后在循环后面*/
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());

        return executor;
    }

}
