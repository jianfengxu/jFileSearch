package net.qqxh.service.task;

import net.qqxh.persistent.FileNode;
import net.qqxh.persistent.Jfile;
import net.qqxh.service.FileResolveService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Component
public class FileReadTask {
    @Autowired
    FileResolveService fileResolveService;
    @Autowired
    private RedisTemplate<String, FileNode> fileNodeRedisTemplate;
    private final static Logger logger = LoggerFactory.getLogger(FileReadTask.class);
    String fnrkey = "file_node_list";

    @Async("fileReadExecutor")
    public void readFile2SimpleDate(String path, IOFileFilter fileFilter, IOFileFilter dirFilter,FileReadTask fileReadTask,String root) {
        Thread th=Thread.currentThread();
        logger.info(System.currentTimeMillis()+"................."+th.getId()+".................."+th.getName());
        File file = new File(path);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    File f = files[i];
                    if (f.isDirectory()) {
                        if (StringUtils.equals(".svn", f.getName())) {
                            continue;
                        }
                      /*  Collection fs = FileUtils.listFiles(f, fileFilter, dirFilter);
                        if (fs == null || fs.size() == 0) {
                            continue;
                        }*/
                        fileReadTask.readFile2SimpleDate(f.getPath(), fileFilter, dirFilter,fileReadTask,root);
                        putFileNode2Redis(f,root);
                    } else {
                        if (f.getName().startsWith("~$")) {
                            continue;
                        }
                        if (fileFilter.accept(f)) {
                            putFileNode2Redis(f,root);
                        }

                    }

                }
            }
        } else {
            putFileNode2Redis(file,root);

        }
    }




    private void putFileNode2Redis(File file,String root) {
        FileNode fn =new FileNode(file,root);
        fileNodeRedisTemplate.opsForList().leftPush(fnrkey, fn);

    }


    @Async("fileReadExecutor")
    public void readFile2FmartDate(FileNode fileNode, List<FileNode> tops, IOFileFilter fileFilter, IOFileFilter dirFilter, FileReadTask fileReadTask,String root) {
        Thread th=Thread.currentThread();
        logger.info(System.currentTimeMillis()+"................."+th.getId()+".................."+th.getName());
        File file = new File(fileNode.getId());
        if (file.isDirectory()) {
            if(fileNode.getSons()==null){
                fileNode.setSons(new ArrayList<>());
            }
            File[] files = file.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    File f = files[i];
                    FileNode fn =new FileNode(f,root);
                    if (f.isDirectory()) {
                        if (StringUtils.equals(".svn", f.getName())) {
                            continue;
                        }
                        fileReadTask.readFile2FmartDate(fn,tops,fileFilter, dirFilter,fileReadTask,root);
                        fileNode.getSons().add(fn);
                    } else {
                        if (f.getName().startsWith("~$")) {
                            continue;
                        }
                        if (fileFilter.accept(f)) {
                            fileNode.getSons().add(fn);

                        }
                    }
                    if(fn.getParentId().equals("root")){
                        tops.add(fn);
                    }
                }
            }

        } else {
            tops.add(fileNode);
        }
    }
}
