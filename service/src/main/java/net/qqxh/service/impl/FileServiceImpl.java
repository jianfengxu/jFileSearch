package net.qqxh.service.impl;

import com.google.gson.Gson;
import net.qqxh.common.utils.FileAnalysisTool;

import net.qqxh.persistent.JfSysUserData;
import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import net.qqxh.service.FileEsService;
import net.qqxh.service.FileService;
import net.qqxh.service.common.BaseCallBack;

import net.qqxh.service.task.FileReadTask;
import net.qqxh.service.task.FileResolveTask;
import net.qqxh.service.task.FileResolveTaskCallBack;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.*;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.elasticsearch.common.text.Text;
import net.qqxh.persistent.FileNode;

import java.io.File;
import java.io.IOException;
import java.util.*;


@Service
public class FileServiceImpl implements FileService {

    private static String ES_TYPE = "content";

    @Autowired
    private TransportClient esclient;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private RedisConnectionFactory redisConnectionFactory;

    @Value("${SUPPORT_EXTENSIONS}")
    private String[] supportExtensions;
    @Value("${jfile.auto_caching}")
    private boolean aotuCaching;
    @Autowired
    FileResolveTask fileResolveTask;

    @Autowired
    FileEsService fileEsService;
    @Autowired
    private RedisTemplate<String, FileNode> fileNodeRedisTemplate;
    @Autowired
    private FileReadTask fileReadTask;
    String fnrkey = "file_node_list";
    private Map<String, Object> libs = new HashMap();


    @Override
    public List<Map> queryFileFromES(String esIndex, String keyword, String path, Integer esFrom, Integer esSize) {
        String highLightField = "content";
        QueryBuilder matchQueryc = QueryBuilders.matchPhraseQuery("content", keyword);
        QueryBuilder matchQueryn = QueryBuilders.matchPhraseQuery("name", keyword);
        QueryBuilder matchQueryPath = QueryBuilders.matchPhraseQuery("path", path);
        BoolQueryBuilder boolQueryMust1 = QueryBuilders.boolQuery();
        if (StringUtils.isNotEmpty(keyword)) {
            boolQueryMust1.should(matchQueryc);
            boolQueryMust1.should(matchQueryn);
        }
        BoolQueryBuilder boolQueryMust2 = QueryBuilders.boolQuery();
        if (StringUtils.isNotEmpty(path)) {
            boolQueryMust2.must(matchQueryPath);
        }
        BoolQueryBuilder boolQueryMust = QueryBuilders.boolQuery();
        boolQueryMust.must(boolQueryMust1).must(boolQueryMust2);
       /* SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        String[] includes = new String[] {"name","path","content"};
        String[] excludes = Strings.EMPTY_ARRAY;
        FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);
        searchSourceBuilder.fetchSource(fetchSourceContext);*/
        HighlightBuilder hiBuilder = new HighlightBuilder();
        hiBuilder.preTags("<span class='high_light'>");
        hiBuilder.postTags("</span>");
        hiBuilder.field(highLightField);
        // 搜索数据
        SearchResponse response = esclient.prepareSearch(esIndex)
                .setQuery(boolQueryMust).setFrom(esFrom).setSize(esSize)
                .highlighter(hiBuilder)
                .execute().actionGet();
        //获取查询结果集
        SearchHits searchHits = response.getHits();
        System.out.println("共搜到:" + searchHits.getTotalHits() + "条结果!");
        List<Map> list = new ArrayList();
        for (SearchHit searchHit : searchHits.getHits()) {
            Map jfileMap = searchHit.getSourceAsMap();
            HighlightField highlightField = searchHit.getHighlightFields().get(highLightField);
            Text[] text = highlightField == null ? new Text[]{} : highlightField.getFragments();
            StringBuffer content = new StringBuffer();
            for (Text str : text) {
                content.append(str);
            }
            String textContent = content.toString();
            jfileMap.put("text", textContent);
            jfileMap.put("rid", searchHit.getId());
            list.add(jfileMap);
        }
        return list;
    }

    @Override
    public void resolveAllFile(SearchLib searchLib, FileResolveTaskCallBack fileResolveTaskCallBack) throws IOException {
        resolveFileByPath(searchLib, searchLib.getFileSourceDir(), fileResolveTaskCallBack);
    }

    @Override
    public void resolveFileByPath(SearchLib searchLib, String path, FileResolveTaskCallBack fileResolveTaskCallBack) throws IOException {
        File file = new File(path);
        if (file.isDirectory()) {
            IOFileFilter fileFilter;
            if (supportExtensions == null) {
                fileFilter = TrueFileFilter.INSTANCE;
            } else {
                String[] suffixes = toSuffixes(supportExtensions);
                fileFilter = new SuffixFileFilter(suffixes);
            }
            /*进行递归*/
            IOFileFilter dirFileFilter = HiddenFileFilter.VISIBLE;
            Collection<File> files = FileUtils.listFiles(file, fileFilter, dirFileFilter);
            Integer totalCount = files.size();
            Iterator<File> iterator = files.iterator();
            do {
                if (iterator.hasNext()) {
                    /*过滤掉window系统临时文件*/
                    File f = iterator.next();
                    if (f.getName().startsWith("~$")) {
                        totalCount--;
                        continue;
                    }
                    Jfile jfile = new Jfile(f);
                    fileResolveTask.doFileResolve(searchLib, jfile, fileResolveTaskCallBack, totalCount);
                }
            } while (iterator.hasNext());
        } else {
            Jfile jfile = new Jfile(file);
            fileResolveTask.doFileResolve(searchLib, jfile, fileResolveTaskCallBack, 1);
        }
    }

    @Override
    public String deleteIndex(SearchLib searchLib) {

        IndicesExistsRequest request = new IndicesExistsRequest(searchLib.getEsIndex());
        IndicesExistsResponse response =  esclient.admin().indices().exists(request).actionGet();
        if(response.isExists()){
            DeleteIndexResponse dResponse = esclient.admin().indices().prepareDelete(searchLib.getEsIndex())
                    .execute().actionGet();
        }
        return "success";
    }


    /**
     * 清空redis缓存
     *
     * @return
     */
    @Override
    public String deleteRedis(String index) {
        redisConnectionFactory.getConnection().flushAll();
        return "success";
    }

    @Override
    public Object readFile2SimpleData() {
        long size = fileNodeRedisTemplate.opsForList().size(fnrkey);
        List<FileNode> nodes;
        if (size == 0) {
            nodes = new ArrayList<>();
        } else {
            nodes = fileNodeRedisTemplate.opsForList().range(fnrkey, 0, size);
        }
        Gson gson = new Gson();

        return gson.toJson(nodes);
    }

    @Override
    public Object readFile2FormatTree(String key) {
        return libs.get(key);
    }

    private List<FileNode> formatTree(List<FileNode> datalist) {
        Iterator<FileNode> iterator = datalist.iterator();
        List<FileNode> tops = new ArrayList<FileNode>();
        while (iterator.hasNext()) {
            Gson gson = new Gson();
            FileNode s = iterator.next();
            //去掉级联关系后需要手动维护这个属性
            boolean getParent = false;
            for (FileNode p : datalist) {
                if (p.getId().equals(s.getParentId())) {
                    List sonList = p.getSons();
                    if (sonList == null) {
                        p.setSons(new ArrayList());
                    }
                    p.getSons().add(s);
                    getParent = true;
                    break;
                }
            }
            if (!getParent) {
                tops.add(s);
            }
        }
        return tops;
    }

    @Override
    public boolean findFileByViewPath(SearchLib searchLib, String viewPath) {
        String src = FileAnalysisTool.getFileSourceByView(searchLib.getFileSourceDir(), searchLib.getFileViewDir(), viewPath);
        File file = new File(src);
        return file.exists();
    }

    @Override
    public void cleanupViewFile(SearchLib searchLib, BaseCallBack baseCallBack) {
        long start = System.currentTimeMillis();
        File dir = new File(searchLib.getFileViewDir());
        IOFileFilter fileFilter = TrueFileFilter.INSTANCE;
        IOFileFilter dirFilter = HiddenFileFilter.VISIBLE;
        Collection<File> files = FileUtils.listFiles(dir, fileFilter, dirFilter);
        int totalCount = files.size() + 1;
        Iterator<File> iterator = files.iterator();
        do {
            if (iterator.hasNext()) {
                File file = iterator.next();
                String viewPath = file.getPath();
                Map msg = new HashMap<>();
                if (findFileByViewPath(searchLib, viewPath)) {
                    msg.put("status", 2);
                } else {
                    if (file.delete()) {
                        /*删除es、redis数据*/
                        String srcPath = FileAnalysisTool.getFileSourceByView(searchLib.getFileSourceDir(), searchLib.getFileViewDir(), viewPath);
                        List<Jfile> list = fileEsService.findFileByPath(searchLib, srcPath);
                        for (Jfile oldJfile : list) {
                            fileEsService.deleteFileFromEs(searchLib.getEsIndex(), oldJfile.getRid());
                            stringRedisTemplate.delete(oldJfile.getRid());
                        }
                        System.out.println(file.getPath() + "删除成功");
                        msg.put("status", 1);
                    } else {
                        System.out.println(file.getPath() + "删除失败");
                        msg.put("status", 0);
                    }
                }
                msg.put("totalCount", totalCount);
                msg.put("path", viewPath);
                baseCallBack.doCallBack(msg);
            }
        } while (iterator.hasNext());

        cleanDirectory(dir);

        long end = System.currentTimeMillis();
        Map msg = new HashMap<>();
        msg.put("totalCount", totalCount);
        msg.put("timeCost", end - start);
        baseCallBack.doCallBack(msg);
    }

    /**
     * 清理空文件夹
     *
     * @param dir
     */
    private void cleanDirectory(File dir) {
        File[] dirs = dir.listFiles();
        for (File file : dirs) {
            if (file.isDirectory()) {
                cleanDirectory(file);
            }
        }
        if (dir.isDirectory()) {
            if (dir.delete()) {
                System.out.println(dir.getPath() + "删除成功");
            }
        }
    }

    /**
     * 每30分钟刷新一次缓存
     *
     * @return
     */
    @Override
    @Scheduled(fixedRate = 1000 * 60 * 30)
    public void resolveFliesList2TreeNode() {
        if (!aotuCaching) {
            aotuCaching = true;
            return;
        }
        IOFileFilter fileFilter;
        if (supportExtensions == null) {
            fileFilter = TrueFileFilter.INSTANCE;
        } else {
            String[] suffixes = toSuffixes(supportExtensions);
            fileFilter = new SuffixFileFilter(suffixes);
        }
        IOFileFilter dirFileFilter = HiddenFileFilter.VISIBLE;
        Map<String, SearchLib> searchLibMap = JfSysUserData.getSearchLibMap();
        Map<String, Object> newlibs = new HashMap();
        for (Map.Entry entry : searchLibMap.entrySet()) {
            SearchLib searchLib = (SearchLib) entry.getValue();
            File file = new File(searchLib.getFileSourceDir());
            if (!file.exists()) {
                file.mkdir();
            }
            FileNode fileNode = new FileNode(new File(searchLib.getFileSourceDir()), searchLib.getFileSourceDir());
            List newTops = new ArrayList();
            fileReadTask.readFile2FmartDate(fileNode, newTops, fileFilter, dirFileFilter, fileReadTask, searchLib.getFileSourceDir());
            newlibs.put(searchLib.getFileSourceDir(), newTops);
        }
        libs = newlibs;
    }

    private void putFileNode2Redis(SearchLib searchLib, File file) {
        FileNode fn = new FileNode(file, searchLib.getFileSourceDir());
        fileNodeRedisTemplate.opsForList().leftPush(fnrkey, fn);
    }


    private static String[] toSuffixes(String[] extensions) {
        String[] suffixes = new String[extensions.length];

        for (int i = 0; i < extensions.length; ++i) {
            suffixes[i] = "." + extensions[i];
        }

        return suffixes;
    }

    @Override
    public void sendToDesktop(SearchLib searchLib, String path, String userid) {
        FileNode fn = new FileNode(new File(path), searchLib.getFileSourceDir());
        removeFromDesktop(searchLib, path, userid);
        fileNodeRedisTemplate.opsForList().leftPush(fnrkey + "-" + userid, fn);
    }

    @Override
    public void removeFromDesktop(SearchLib searchLib, String path, String userid) {
        FileNode fn;
        if (path == null) {
            fn = new FileNode();

        } else {
            fn = new FileNode(new File(path), searchLib.getFileSourceDir());
        }

        Long res = fileNodeRedisTemplate.opsForList().remove(fnrkey + "-" + userid, 1, fn);

    }

    @Override
    public List<FileNode> getMyDesktop(String userid) {
        long size = fileNodeRedisTemplate.opsForList().size(fnrkey + "-" + userid);
        List<FileNode> nodes;
        if (size == 0) {
            nodes = new ArrayList<>();
        } else {
            nodes = fileNodeRedisTemplate.opsForList().range(fnrkey + "-" + userid, 0, size);
        }
        return nodes;
    }

    @Override
    public void clearDesktop(String userid) {
        fileNodeRedisTemplate.delete(fnrkey + "-" + userid);
    }
}
