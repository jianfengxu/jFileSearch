package net.qqxh.service.impl;

import com.google.gson.Gson;
import net.qqxh.common.utils.FileAnalysisTool;
import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import net.qqxh.persistent.h2.JfileRepository;
import net.qqxh.resolve2text.File2TextResolve;
import net.qqxh.resolve2text.File2TextResolveFactory;
import net.qqxh.resolve2text.exception.File2TextResolveNotFondException;
import net.qqxh.resolve2view.File2ViewResolve;
import net.qqxh.resolve2view.File2ViewResolveFactory;
import net.qqxh.resolve2view.exception.File2ViewResolveNotFondException;
import net.qqxh.service.FileEsService;
import net.qqxh.service.FileResolveService;
import net.qqxh.service.task.FileResolveTask;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class FileResolveServiceImpl implements FileResolveService {
    private final static Logger logger = LoggerFactory.getLogger(FileResolveTask.class);
    @Autowired
    private File2TextResolveFactory file2TextResolveFactory;
    @Autowired
    private File2ViewResolveFactory file2ViewResolveFactory;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private FileEsService fileEsService;
    @Autowired
    JfileRepository jfileRepository;

    private static String FILE_RESOLVED_2_TEXT = "1";
    private static String FILE_RESOLVED_2_VIEW = "2";
    private static String FILE_RESOLVED_2_SHOW = "3";

    @Override
    public String resolveFile2View(String filePath, SearchLib searchLib, String type) {
        File2ViewResolve file2ViewResolve = null;

        try {
            file2ViewResolve = file2ViewResolveFactory.getFile2ViewResolve(type);
        } catch (File2ViewResolveNotFondException e) {
            e.printStackTrace();
            return null;
        }
        String toPath = FileAnalysisTool.getOffSiteViewDir(searchLib.getFileSourceDir(), searchLib.getFileViewDir(), filePath, file2ViewResolve.getviewFix());
        return file2ViewResolve.resolve(filePath, toPath);
    }

    @Override
    public String resolveFile2Text(byte[] file, String type) {
        File2TextResolve file2TextResolve = null;
        try {
            file2TextResolve = file2TextResolveFactory.getFileResolve(type);
        } catch (File2TextResolveNotFondException e) {
            e.printStackTrace();
            return null;
        }
        return file2TextResolve.resolve(file);
    }

    @Override
    public void deleteFileFromRedis(String rid) {
        stringRedisTemplate.delete(rid);
    }

    public Jfile findFileByPath(String path) {

        return null;
    }

    @Override
    public boolean resolve(SearchLib searchLib, Jfile jfile) throws IOException {
        String path = jfile.getPath();
        String fileName = jfile.getName();
        String suffix = jfile.getFix();
        File file = new File(path);
        byte[] data = FileUtils.readFileToByteArray(file);
        String content = "";
        if (data.length > 0) {
            content = resolveFile2Text(data, suffix);
        }
        /*插入之前先删除*/
        List<Jfile> list = fileEsService.findFileByPath(searchLib, path);
        for (Jfile oldJfile : list) {
            logger.warn("发现老文件数据正在清理中，文件路径：" + path);
            fileEsService.deleteFileFromEs(searchLib.getEsIndex(), oldJfile.getRid());
        }
        /*支持查看*/
        String viewPath = resolveFile2View(path, searchLib, suffix);
        String viewFix = FileAnalysisTool.getFileFix(viewPath);
        /*支持检索*/
        fileEsService.addFile2ES(searchLib.getEsIndex(), content, fileName, path, viewFix);
        return true;
    }


}
